import Taro, {Component} from '@tarojs/taro'
import {View, Text} from '@tarojs/components'


export default class Dialog extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View className='index'>
        弹框
        {
          this.props.children
        }
      </View>
    )
  }
}
