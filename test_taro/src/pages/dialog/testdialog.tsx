import Taro, {Component} from '@tarojs/taro'
import {View, Text, Button} from '@tarojs/components'
import Dialog from './dialog'

const isH5 = process.env.TARO_ENV == 'h5';   // 可以判断当前环境

export default class Testdialog extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '张三'
    }
  }
  componentWillMount() {
    console.log(isH5)
  }

  test(e) {
    console.log(arguments)
  }
  render() {
    return (
      <View className='index'>
        <Dialog>
          <Text>text 传入</Text>
        </Dialog>
        <Dialog></Dialog>
        <Dialog></Dialog>
        <Dialog></Dialog>
        <Button onClick={this.test.bind(this, this.state.name)}>测试事件</Button>
      </View>
    )
  }
}
