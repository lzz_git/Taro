import Taro, {Component, Config} from '@tarojs/taro'
import {View, Button} from '@tarojs/components'
import './index.scss'
import AddQuestion from './addQuestion'

export default class Index extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showQuestionModal: false,  // 显示图层
      questionList: []
    }
  }

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '首页'
  }

  componentWillMount() {
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  componentDidShow() {
  }

  componentDidHide() {
  }
  // 显示弹框
  addQuestion() {
    this.setState({showQuestionModal: true})
  }
  // 关闭弹框
  closeQuestion() {
    this.setState({showQuestionModal: false})
  }
  //更新数据
  reveiveQuestion(options) {
    let {questionList} = this.state;
    questionList.push(options);
    this.setState({questionList: questionList}, () => {
      console.log(this.state.questionList)
    });
    this.closeQuestion();

  }
  render() {

    return (
      <View className='index'>
        <View className='title'>Taro 问答实例</View>
        {showQuestionModal ? <AddQuestion onCloseQuestion={this.closeQuestion.bind(this)} onReveiveQuestion={this.reveiveQuestion.bind(this)} /> : null}
        <Button onClick={this.addQuestion.bind(this)} className='btn-question'>提问</Button>
      </View>
    )
  }
}
