import Taro, {Component} from '@tarojs/taro'
import {View, Input, Textarea, Button} from '@tarojs/components'
import Dialog from './dialog'
import './addquestion.scss'

export default class AddQuestion extends Component {
  btnCancel() {
  //  需要调用上层组件的方法
    this.props.onCloseQuestion && this.props.onCloseQuestion();
  }
  // 点击确定并进行空内容验证
  btnOk() {
  //  点击确定时 采集数据 关闭窗体传递数据
    if (this.state.title && this.state.desc) {
    // 调用父层组件 传递数据
      this.props.onReveiveQuestion && this.props.onReveiveQuestion(this.state);
    } else {
      Taro.showToast({title: '请输入标题或描述', icon: 'none'})
    }
  }
  // 标题输入内容
  changeTitle(event) {
    this.setState({title: event.target.value})
  }
  // 文本域输入内容
  changeDesc(event) {
    this.setState({desc: event.target.value})
  }

  render() {
    return (
      <Dialog>
        <View className='add-question'>
          <View className='question-body'>
            <Input focus onInput={this.changeTitle.bind(this)} placeholder='请输入您的问题标题' className='question-title' />
            <Textarea onInput={this.changeDesc.bind(this)} placeholder='请输入问题描述' className='question-desc'></Textarea>
            <View className='btn-group'>
              <Button onClick={this.btnOk.bind(this)} className='btn-question ok'>确定</Button>
              <Button onClick={this.btnCancel.bind(this)} className='btn-question cancel'>取消</Button>
            </View>
          </View>
        </View>
      </Dialog>
    )
  }
}
